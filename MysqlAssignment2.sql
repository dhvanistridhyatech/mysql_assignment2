MariaDB [(none)]> use query;
Database changed
/*1.The members details arranged in decreasing order of Date of Birth........*/

MariaDB [query]> SELECT date_of_birth FROM members ORDER BY date_of_birth DESC;
+---------------+
| date_of_birth |
+---------------+
| 1984-02-14    |
| 1980-07-12    |
| 1980-06-23    |
+---------------+

/*2.The members details sorting using two columns(gender,date_of_birth); the first one is sorted in ascending order by default while the second column is sorted in descending order
*/

MariaDB [query]> SELECT membership_number,fullname,gender,date_of_birth,physical_address,postal_address,contact_number,email  FROM members ORDER BY `gender`,`date_of_birth` DESC;
+-------------------+-------------------+--------+---------------+------------------+----------------+----------------+----------------+
| membership_number | fullname          | gender | date_of_birth | physical_address | postal_address | contact_number | email          |
+-------------------+-------------------+--------+---------------+------------------+----------------+----------------+----------------+
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23    | NULL           |              1 | NULL           |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123      | NULL           |              1 | jj@fstreet.com |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34    | NULL           |          12345 | rm@street.com  |
+-------------------+-------------------+--------+---------------+------------------+----------------+----------------+----------------+
3 rows in set (0.000 sec)

3 rows in set (0.000 sec)
/*3.The members details get the unique values for genders.....................*/

MariaDB [query]> SELECT DISTINCT gender FROM members;
+--------+
| gender |
+--------+
| female |
| Male   |
+--------+
2 rows in set (0.001 sec)

/*4.The movies detials get a list of movie category_id  and corresponding years in which they were released......*/

MariaDB [query]> SELECT category_id,year_released FROM `movies`;
+-------------+---------------+
| category_id | year_released |
+-------------+---------------+
| 1           |          2011 |
| 2           |          2008 |
| NULL        |          2008 |
| NULL        |          2010 |
| 8           |          2007 |
| 6           |          2007 |
| 6           |          2007 |
| 8           |          2005 |
| NULL        |          2012 |
+-------------+---------------+
9 rows in set (0.000 sec)

/*5.The members table - get total number of males and females.....*/

MariaDB [query]> SELECT COUNT(CASE WHEN UPPER(gender) = 'MALE' THEN 1 END) Male, COUNT(CASE WHEN UPPER(gender) = 'FEMALE' THEN 1 END) Female, COUNT(CASE WHEN gender IS NULL THEN 1 END),COUNT(membership_number)FROM members;
+------+--------+--------------------------------------------+--------------------------+
| Male | Female | COUNT(CASE WHEN gender IS NULL THEN 1 END) | COUNT(membership_number) |
+------+--------+--------------------------------------------+--------------------------+
|    1 |      3 |                                          0 |                        4 |
+------+--------+--------------------------------------------+--------------------------+
1 row in set (0.000 sec)

/*6.The movies table - get all the movies that have the word "code" as part of the title, we would use the percentage wildcard to perform a pattern match on both sides of the word "code"......*/

MariaDB [query]> SELECT movie_id,title,director,year_released,category_id FROM movies WHERE title LIKE '%code%';
+----------+-----------------+------------+---------------+-------------+
| movie_id | title           | director   | year_released | category_id |
+----------+-----------------+------------+---------------+-------------+
|        4 | CODE-NAME-BLACK | Edgar Jimz |          2010 | NULL        |
|        7 | Divinci Code    | NULL       |          2007 | 6           |
+----------+-----------------+------------+---------------+-------------+
2 rows in set (0.000 sec)

/*7.The movies table - get all the movies that were released in the year "200X" (using _ underscore wildcard)...*/

MariaDB [query]> SELECT movie_id,title,director,year_released,category_id FROM movies WHERE year_released LIKE '200_';
+----------+----------------------+-------------------+---------------+-------------+
| movie_id | title                | director          | year_released | category_id |
+----------+----------------------+-------------------+---------------+-------------+
|        2 | Forgetting Sarah Mar | Nocholas  stoller |          2008 | 2           |
|        3 | X-MEN                | NULL              |          2008 | NULL        |
|        5 | DADDAY'S LITTLE GIRL | NULL              |          2007 | 8           |
|        6 | Angels and Demons    | NULL              |          2007 | 6           |
|        7 | Divinci Code         | NULL              |          2007 | 6           |
|        9 | Honey Monners        | John Schultz      |          2005 | 8           |
+----------+----------------------+-------------------+---------------+-------------+
6 rows in set (0.000 sec)

/*8.The movies table - get movies that were not released in the year 200x.....*/
MariaDB [query]> SELECT movie_id,title,director,year_released,category_id FROM movies WHERE year_released NOT LIKE '200_';
+----------+----------------------+--------------+---------------+-------------+
| movie_id | title                | director     | year_released | category_id |
+----------+----------------------+--------------+---------------+-------------+
|        1 | Pirates of the Carib | Rob Marshell |          2011 | 1           |
|        4 | CODE-NAME-BLACK      | Edgar Jimz   |          2010 | NULL        |
|       16 | 67%Guilty            | NULL         |          2012 | NULL        |
+----------+----------------------+--------------+---------------+-------------+
3 rows in set (0.000 sec)

/*9.The movies table  - movie titles in upper case letters.........*/
MariaDB [query]> SELECT UPPER(title) FROM movies;
+----------------------+
| UPPER(title)         |
+----------------------+
| PIRATES OF THE CARIB |
| FORGETTING SARAH MAR |
| X-MEN                |
| CODE-NAME-BLACK      |
| DADDAY'S LITTLE GIRL |
| ANGELS AND DEMONS    |
| DIVINCI CODE         |
| HONEY MONNERS        |
| 67%GUILTY            |
+----------------------+
9 rows in set (0.000 sec)

/*10.Create new table "movierentals" (see movierentals.png image)*................alreday created*/

/*11.The movierentals table - get the number of times that the movie with id 2 has been rented out...*/
MariaDB [query]> SELECT COUNT(`movie_id`)  FROM `movierentals` WHERE `movie_id` = 2;
+-------------------+
| COUNT(`movie_id`) |
+-------------------+
|                 3 |
+-------------------+
1 row in set (0.000 sec)

/*12.The movierentals table - omits duplicate records which have same movie_id.....*/

MariaDB [query]> SELECT DISTINCT `movie_id` FROM `movierentals`;
+----------+
| movie_id |
+----------+
|        1 |
|        2 |
|        3 |
+----------+
3 rows in set (0.000 sec)

/*13.The movie table - latest movie year released(using MAX function)....*/

MariaDB [query]> SELECT MAX(`year_released`)  FROM `movies`;
+----------------------+
| MAX(`year_released`) |
+----------------------+
|                 2012 |
+----------------------+
1 row in set (0.001 sec)

/*14.Create "payments" table (see payments.png image)....alreday created.*/

/*15.The payments table - find the average amount paid..........*/
MariaDB [query]> SELECT AVG(amount), COUNT(*) FROM payment;
+-------------+----------+
| AVG(amount) | COUNT(*) |
+-------------+----------+
|   1883.3333 |        6 |
+-------------+----------+
1 row in set (0.000 sec)

/*16.Add a new field to the members table...........*/

MariaDB [query]> ALTER TABLE members DROP COLUMN columntest;
Query OK, 0 rows affected (0.015 sec)
Records: 0  Duplicates: 0  Warnings: 0

/*17.Delete a database from MySQL server (DROP command is used )..........*/

MariaDB [query]> drop table if exists MOVIES;
Query OK, 0 rows affected (0.031 sec)

/*18.Delete an object (like Table , Column)from a database.(DROP command is used )....*/

MariaDB [query]> DELETE FROM members WHERE membership_number = 1;
Query OK, 1 row affected (0.004 sec)

MariaDB [query]> TRUNCATE TABLE movies;
Query OK, 0 rows affected (0.049 sec)

/*19.DROP a table from Database............*/

MariaDB [query]> DROP  table movies from Database;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'from Database' at line 1
MariaDB [query]> DROP  movies FROM Database;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'movies FROM Database' at line 1

/*20.Rename table `movierentals` TO `movie_rentals`;.........*/
MariaDB [query]> RENAME TABLE `movierentals` TO `movie_rentals`;
Query OK, 0 rows affected (0.027 sec)

/*21.The member table  - changes the width of "fullname" field from 250 to 50....*/
MariaDB [query]> ALTER TABLE `members` CHANGE COLUMN `full_names` `fullname` char(50) NOT NULL;
Query OK, 3 rows affected (0.111 sec)
Records: 3  Duplicates: 0  Warnings: 0

/*22.The member table  - Getting a list of ten (10) members only from the database......*/
MariaDB [query]> SELECT membership_number,fullname,gender,date_of_birth,physical_address,postal_address,contact_number,email  FROM members LIMIT 10;
+-------------------+-------------------+--------+---------------+------------------+----------------+----------------+----------------+
| membership_number | fullname          | gender | date_of_birth | physical_address | postal_address | contact_number | email          |
+-------------------+-------------------+--------+---------------+------------------+----------------+----------------+----------------+
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123      | NULL           |              1 | jj@fstreet.com |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34    | NULL           |          12345 | rm@street.com  |
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23    | NULL           |              1 | NULL           |
+-------------------+-------------------+--------+---------------+------------------+----------------+----------------+----------------+
3 rows in set (0.001 sec)

/*23.The member table - gets data starting the second row and limits the results to 2..........*/
MariaDB [query]> SELECT membership_number,fullname,gender,date_of_birth,physical_address,postal_address,contact_number,email  FROM members LIMIT 1,2;
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+---------------+
| membership_number | fullname         | gender | date_of_birth | physical_address | postal_address | contact_number | email         |
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+---------------+
|                 3 | Robert Fill      | Male   | 1980-07-12    | 3rd street 34    | NULL           |          12345 | rm@street.com |
|                 4 | Gloaria Williams | female | 1984-02-14    | 2nd street 23    | NULL           |              1 | NULL          |
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+---------------+
2 rows in set (0.001 sec)


